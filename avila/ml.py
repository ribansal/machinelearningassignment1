import os, sys
path = os.path.join(sys.path[0],'lib')
sys.path.append(path)
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import lib
import lib.knn as knn
import lib.dtclassifier as dt
import lib.svm as svm 
import numpy as np
import lib.ann as ann
import lib.boosting as boosting
import pandas as pd
from sklearn.preprocessing import LabelEncoder


def main():
    #generate classification data
    X,y = generate_data()
    X_train, X_test, y_train, y_test = preprocess_data(X,y)
    
    #run knn algorithm
    knn.run_knn(X_train, X_test, y_train, y_test)
    
    #run decision tree
    dt.run_dt(X_train, X_test, y_train, y_test)
    
    #run svm
    svm.run_compare(X_train, X_test, y_train, y_test)
    svm.run_svm(X_train, X_test, y_train, y_test)
    svm.run_svm_poly(X_train, X_test, y_train, y_test)
    
    #run neural network
    ann.run_ann(X_train, X_test, y_train, y_test)
    
    #run boosting
    boosting.run_boosting(X_train, X_test, y_train, y_test)
    


def preprocess_data(X,y):    
    #preprocess data in training
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1, stratify=y)
    return X_train, X_test, y_train, y_test

def generate_data():

    np.random.seed(151)
    names = ['intercolumnardistance', 'uppermargin' , 'lowermargin', 'exploitation' , 'rownumber' , 'modularratio' , 'interlinearspacing' , 'weight' , 'peaknumber' , 'modularratiointerlinearspacing', 'class']
    
    dataset = pd.read_csv('avila-tr.txt', names = names)
    #print(dataset.head())
    #preprocessing
    X = dataset.iloc[:, :-1]
    y = dataset.iloc[:, -1]
    
    lencoder = LabelEncoder()
    y = lencoder.fit_transform(y.values)
    y = pd.DataFrame(y)

    y = y.values.ravel()
    X = X.values 
    return X,y

    
if __name__ == '__main__':
    main()
