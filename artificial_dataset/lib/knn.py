from drawcurves import *
from sklearn.metrics import classification_report, confusion_matrix


def run_knn(X_train, X_test, y_train, y_test):
    
    np.random.seed(151)
    #print(dataset.head())
    
    log = open("logfileKNN.txt", "w")
    
    
    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1, stratify=y)
    log.write("\n")
    log.write("*"*80)
    log.write("\ntraining set size is {}\ntest set size is {}".format(str(X_train.shape), str(X_test.shape)))
    log.write("\ntraining set output size is {}\ntest set output size is {}".format(str(y_train.shape), str(y_test.shape)))
    
    
    pr_pipe = make_pipeline(StandardScaler(), KNeighborsClassifier())

    #learning curve
    draw_learning_curve(pr_pipe, X_train, y_train, log, "KNN")
    
    #validation curve
    param_range = np.arange(1,40, 1)
    param_name = "kneighborsclassifier__n_neighbors"
    param_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    
    
    #validation curve 2
    param_name2 = 'kneighborsclassifier__metric'
    param_range2 = ['euclidean','manhattan','chebyshev','minkowski'] 
    param_opt_value2 = draw_validation_curve(pr_pipe, param_range2, param_name2, X_train, y_train, log)
    
    #making classifier with best values
    pr_pipe = make_pipeline(StandardScaler(), KNeighborsClassifier(n_neighbors=param_opt_value, metric=param_opt_value2))
    
    pr_pipe.fit(X_train, y_train)
    
    #predict on test features
    prediction = pr_pipe.predict(X_test)
    
    #accuracy
    print("prediction accuracy is ", pr_pipe.score(X_test, y_test)*100, "%")
    log.write("prediction accuracy in KNN is {} \n".format(pr_pipe.score(X_test, y_test)*100))

    #writing confusion matrix
    predictions = pr_pipe.predict(X_train)
    print(predictions)
    print(confusion_matrix(y_train,predictions))
    print(classification_report(y_train,predictions))
    log.write("confusion and classification on training data\n")
    log.write("%s \n"%confusion_matrix(y_train,predictions))
    log.write("%s \n"%classification_report(y_train,predictions))
    predictions = pr_pipe.predict(X_test)
    print(confusion_matrix(y_test,predictions))
    print(classification_report(y_test,predictions))
    log.write("confusion and classification on test data\n")
    log.write("%s \n"%confusion_matrix(y_test,predictions))
    log.write("%s \n"%classification_report(y_test,predictions))
    
    log.close()