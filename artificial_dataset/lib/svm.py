import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import learning_curve
from sklearn.model_selection import validation_curve
from drawcurves import *

def run_compare(X_train, X_test, y_train, y_test):
    np.random.seed(151)
    log = open("logfileSVMcompare.txt", "w")
    
    svckernel=['linear', 'poly', 'rbf', 'sigmoid']
    kcolor=['red', 'green', 'blue','black']
    i=0
    ax = plt.subplot(111)
    for k in svckernel:
        
        log.write("\nMaking learning curve with {} kernel".format(k))
        svc = svm.SVC(kernel=k)
        pr_pipe = make_pipeline(StandardScaler(), svc)
        #learning curve
        train_sizes, train_scores, test_scores = learning_curve(estimator=pr_pipe,
                                                                    X=X_train,
                                                                    y=y_train,
                                                                    train_sizes=np.linspace(
                                                                    0.1,1.0,10),
                                                                    cv=5,
                                                                    shuffle=True,
                                                                    n_jobs=-1)
        train_mean = np.mean(train_scores, axis=1)
        train_std = np.std(train_scores, axis=1)
        test_mean = np.mean(test_scores, axis=1)
        test_std = np.std(test_scores, axis=1)
        log.write("\ntrain mean =")
        log.write(str(train_mean))
        log.write("\ntest mean = ")
        log.write(str(test_mean))
        log.write("\ntrain sizes = ")
        log.write(str(train_sizes))
        print("train_mean ", train_mean)
        print("test_mean ", test_mean)
        
    
        plt.plot(train_sizes, train_mean, color=kcolor[i], marker='o', markersize=5, 
                label=k+' training accuracy')
        
        plt.plot(train_sizes, test_mean, color=kcolor[i], marker='s', linestyle='dashed' ,
                markersize=5, label=k+' validation accuracy')
    
        plt.grid()
        plt.xlabel('Number of training samples')
        plt.ylabel('Accuracy')
        i+=1
    
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    filename = "LC_SVM_compare.png"
    plt.savefig(filename, bbox_inches='tight')
    log.close()


def run_svm(X_train, X_test, y_train, y_test):
    np.random.seed(151)
    #print(dataset.head())
    log = open("logfileSVM.txt", "w")
    #preprocessing
    
    print(X_train.shape, y_train.shape)
    svc = svm.SVC()
    
    pr_pipe = make_pipeline(StandardScaler(), svc)

    #learning curve
    draw_learning_curve(pr_pipe, X_train, y_train, log, "SVM_rbf")
    
    params = {}
    params["kernel"] = 'rbf'
    
    
    #validation curve 2
    pr_pipe = make_pipeline(StandardScaler(), svm.SVC(**params))
    param_name2 = 'svc__gamma'
    #param_range2 = 10. ** np.arange(-3, 2)
    param_range2 = np.arange(0.1, 1.1,0.1)
    param_opt_value2 = draw_validation_curve(pr_pipe, param_range2, param_name2, X_train, y_train, log)
    params['gamma'] = param_opt_value2
    
    #validation curve
    pr_pipe = make_pipeline(StandardScaler(), svm.SVC(**params))
    param_range = 10. ** np.arange(-3, 3) 
    param_name = "svc__C"
    param_opt_value = draw_logvalidation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['C'] = param_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), svm.SVC(**params))
    
    pr_pipe.fit(X_train, y_train)
    
    #predict on test features
    prediction = pr_pipe.predict(X_test)
    
    #accuracy
    print("prediction accuracy is ", pr_pipe.score(X_test, y_test)*100, "%")
    log.write("prediction accuracy using VC values {} , {}in SVM is {} \n".format(param_opt_value, param_opt_value2, pr_pipe.score(X_test, y_test)*100))
    
    #writing confusion matrix
    predictions = pr_pipe.predict(X_train)
    print(predictions)
    print(confusion_matrix(y_train,predictions))
    print(classification_report(y_train,predictions))
    log.write("confusion and classification on training data\n")
    log.write("%s \n"%confusion_matrix(y_train,predictions))
    log.write("%s \n"%classification_report(y_train,predictions))
    predictions = pr_pipe.predict(X_test)
    print(confusion_matrix(y_test,predictions))
    print(classification_report(y_test,predictions))
    log.write("confusion and classification on test data\n")
    log.write("%s \n"%confusion_matrix(y_test,predictions))
    log.write("%s \n"%classification_report(y_test,predictions))
    
    log.close()

def run_svm_poly(X_train, X_test, y_train, y_test):
    
    np.random.seed(151)
    #print(dataset.head())
    log = open("logfileSVM_poly.txt", "w")
    #preprocessing
    
    print(X_train.shape, y_train.shape)
    svc = svm.SVC(kernel='poly')
    
    pr_pipe = make_pipeline(StandardScaler(), svc)

    #learning curve
    draw_learning_curve(pr_pipe, X_train, y_train, log, "SVM_poly")
    
    params = {}
    params["kernel"] = 'poly'
    
    #validation curve 3
    pr_pipe = make_pipeline(StandardScaler(), svm.SVC(**params))
    param_name3 = 'svc__degree'
    param_range3 = list(range(2,7,1))
    param_opt_value3 = draw_validation_curve(pr_pipe, param_range3, param_name3, X_train, y_train, log)
    params['degree'] = param_opt_value3
    
    
    #validation curve
    pr_pipe = make_pipeline(StandardScaler(), svm.SVC(**params))
    param_range = 10. ** np.arange(-3, 3) 
    param_name = "svc__C"
    param_opt_value = draw_logvalidation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['C'] = param_opt_value
    
    #validation curve
    pr_pipe = make_pipeline(StandardScaler(), svm.SVC(**params))
    param_range = np.arange(-1, 4,1) 
    param_name = "svc__coef0"
    param_coef_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['coef0'] = param_coef_value
    
    """
    #validation curve 2
    pr_pipe = make_pipeline(StandardScaler(), svm.SVC(**params))
    param_name2 = 'svc__gamma'
    param_range2 = 10. ** np.arange(0, 3)
    param_opt_value2 = draw_validation_curve(pr_pipe, param_range2, param_name2, X_train, y_train, log)
    params['gamma'] = param_opt_value2
    """ 
    pr_pipe = make_pipeline(StandardScaler(), svm.SVC(**params))
    
    pr_pipe.fit(X_train, y_train)
    
    #predict on test features
    prediction = pr_pipe.predict(X_test)
    
    #accuracy
    print("prediction accuracy is ", pr_pipe.score(X_test, y_test)*100, "%")
    #log.write("prediction accuracy using VC values C={} , gamma={} and degree={} in SVM is {} %".format(param_opt_value, param_opt_value2, param_opt_value3, pr_pipe.score(X_test, y_test)*100))
    log.write("prediction accuracy using VC values C={}, and degree={} in SVM is {} \n".format(param_opt_value, param_opt_value3, pr_pipe.score(X_test, y_test)*100))
    
    #writing confusion matrix
    predictions = pr_pipe.predict(X_train)
    print(predictions)
    print(confusion_matrix(y_train,predictions))
    print(classification_report(y_train,predictions))
    log.write("confusion and classification on training data\n")
    log.write("%s \n"%confusion_matrix(y_train,predictions))
    log.write("%s \n"%classification_report(y_train,predictions))
    predictions = pr_pipe.predict(X_test)
    print(confusion_matrix(y_test,predictions))
    print(classification_report(y_test,predictions))
    log.write("confusion and classification on test data\n")
    log.write("%s \n"%confusion_matrix(y_test,predictions))
    log.write("%s \n"%classification_report(y_test,predictions))

    log.close()