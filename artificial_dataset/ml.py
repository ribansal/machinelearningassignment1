import os, sys
path = os.path.join(sys.path[0],'lib')
sys.path.append(path)
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import lib
import lib.knn as knn
import lib.dtclassifier as dt
import lib.svm as svm 
import numpy as np
import lib.ann as ann
import lib.boosting as boosting

def main():
    #generate classification data
    X,y = generate_data()
    X_train, X_test, y_train, y_test = preprocess_data(X,y)
    
    #run knn algorithm
    knn.run_knn(X_train, X_test, y_train, y_test)
    
    #run decision tree
    dt.run_dt(X_train, X_test, y_train, y_test)
    
    #run svm
    svm.run_compare(X_train, X_test, y_train, y_test)
    svm.run_svm(X_train, X_test, y_train, y_test)
    svm.run_svm_poly(X_train, X_test, y_train, y_test)
    
    #run neural network
    ann.run_ann(X_train, X_test, y_train, y_test)
    
    #run boosting
    boosting.run_boosting(X_train, X_test, y_train, y_test)

def preprocess_data(X,y):    
    #preprocess data in training
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1, stratify=y)
    return X_train, X_test, y_train, y_test

def generate_data():
    np.random.seed(5000)
    X, y = make_classification(n_samples=4000, n_features=5, n_informative=4, 
                                n_redundant=1, n_repeated=0, n_classes=3, 
                                n_clusters_per_class=2, weights=[0.4,0.3,0.3], flip_y=0.05, 
                                class_sep=1.0, hypercube=True, shift=0.0, scale=1.0, 
                                shuffle=True, random_state=50)

    plt.figure(figsize=(10, 10))
    plt.subplots_adjust(bottom=.05, top=.9, left=.05, right=.95)
    plt.subplot(221)
    plt.scatter(X[:, 0], X[:, 1], marker='o', c=y, s=50, edgecolor='k',cmap='viridis')
    plt.subplot(222)
    plt.scatter(X[:, 0], X[:, 2], marker='o', c=y, s=50, edgecolor='k',cmap='viridis')
    plt.subplot(223)
    plt.scatter(X[:, 0], X[:, 3], marker='o', c=y, s=50, edgecolor='k',cmap='viridis')
    plt.subplot(224)
    plt.scatter(X[:, 0], X[:, 4], marker='o', c=y, s=50, edgecolor='k',cmap='viridis')
    #plt.show()
    plt.savefig('data.png',bbox_inches='tight')
    return X,y

    
if __name__ == '__main__':
    main()
