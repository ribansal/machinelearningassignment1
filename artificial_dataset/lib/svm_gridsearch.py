import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import learning_curve
from sklearn.model_selection import validation_curve
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV

np.random.seed(151)
names = ['lettr', 'x-box', 'y-box', 'width', 'high', 'onpix', 'x-bar', 'y-bar', 'x2bar', 'y2bar', 'xybar', 'x2ybr', 'xy2br', 'x-ege', 'xegvy', 'y-ege', 'yegvx']

dataset = pd.read_csv('../data/letter-recognition.data', names = names)
#print(dataset.head())

#preprocessing
X = dataset.iloc[:, 1:]
y = dataset.iloc[:, 0]

lencoder = LabelEncoder()
y = lencoder.fit_transform(y.values)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=111)

svc = svm.SVC(kernel='poly')


pr_pipe = make_pipeline(StandardScaler(), svc)

C_range = 10. ** np.arange(-3, 5)
degree_range = [1,2,3,4,5]
#gamma_range = 10. ** np.arange(-3, 5)

param_grid = dict(svc__degree=degree_range, svc__C=C_range)
#param_grid = dict(svc__gamma=gamma_range, svc__C=C_range)
grid = GridSearchCV(pr_pipe, param_grid=param_grid, cv=5)

grid.fit(X_train, y_train)

print("The best classifier is: ", grid.best_estimator_)
print("the best score is : ", grid.best_score_)

# plot the scores of the grid
# grid_scores_ contains parameter settings and scores
score_dict = grid.cv_results_

# We extract just the scores
scores = score_dict['mean_test_score']
scores = np.array(scores).reshape(len(C_range), len(degree_range))

# Make a nice figure
plt.figure(figsize=(8, 6))
plt.subplots_adjust(left=0.15, right=0.95, bottom=0.15, top=0.95)
plt.imshow(scores, interpolation='nearest', cmap=plt.cm.get_cmap("Spectral"))
plt.xlabel('degree')
plt.ylabel('C')
plt.colorbar()
plt.xticks(np.arange(len(degree_range)), degree_range, rotation=45)
plt.yticks(np.arange(len(C_range)), C_range)
plt.show()