from drawcurves import *

def run_dt(X_train, X_test, y_train, y_test):
    np.random.seed(151)
    log = open("logfileDT.txt", "w")

    log.write("\n")
    log.write("*"*80)
    log.write("\ntraining set size is {}\ntest set size is {}".format(str(X_train.shape), str(X_test.shape)))
    log.write("\ntraining set output size is {}\ntest set output size is {}".format(str(y_train.shape), str(y_test.shape)))

    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(criterion="gini"))

    #learning curve
    draw_learning_curve(pr_pipe, X_train, y_train, log, "DT")
    
    params = {}
    #plot a validation curve
    print(sorted(pr_pipe.get_params().keys()))
    param_name = "decisiontreeclassifier__criterion"
    param_range = ["gini", "entropy"]
    criterion_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['criterion'] = criterion_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_range = list(range(1,11,1)) 
    param_name = "decisiontreeclassifier__min_samples_leaf"
    min_samples_leaf_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['min_samples_leaf'] = min_samples_leaf_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_range = list(range(2,501,10))
    param_name = "decisiontreeclassifier__max_leaf_nodes"
    max_leaf_nodes_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['max_leaf_nodes'] = max_leaf_nodes_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_name = "decisiontreeclassifier__max_depth"
    param_range = list(range(1,41,1))
    param_name = "decisiontreeclassifier__max_depth"
    max_depth_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['max_depth'] = max_depth_opt_value
    
        

    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_range = 10. ** np.arange(-5, -1)
    param_name = "decisiontreeclassifier__min_samples_split"
    min_samples_split_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['min_samples_split'] = min_samples_split_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_range = np.arange(0.,1.0, 0.1)
    param_name = "decisiontreeclassifier__min_impurity_decrease"
    min_impurity_decrease_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['min_impurity_decrease'] = min_impurity_decrease_opt_value
    
    #params = {}
    #params['criterion'] = criterion_opt_value
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    
    pr_pipe.fit(X_train, y_train)
    
    #predict on test features
    prediction = pr_pipe.predict(X_test)
    
    #accuracy
    print("prediction accuracy is ", pr_pipe.score(X_test, y_test)*100, "%")
    log.write("prediction accuracy in DT is {} %".format(pr_pipe.score(X_test, y_test)*100))
    
    log.close()