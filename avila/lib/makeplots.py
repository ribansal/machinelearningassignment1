import seaborn as sns; sns.set(style="ticks", color_codes=True)
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
names = ['icdistance', 'uppermargin' , 'lowermargin', 'exploitation' , 'rownumber' , 'mratio' , 'ilspacing' , 'weight' , 'peaknumber' , 'mratio_ilspacing', 'class']
dataset = pd.read_csv('../data/avila-tr.txt', names = names)

X = dataset.iloc[:, :-1]
y = dataset.iloc[:, -1]
corrmat = X.corr()
#print(corrmat)
hmap = sns.heatmap(corrmat, square=True, linewidths=.5)
print(hmap.get_ylim())
hmap.set_ylim(10.0, 0)
plt.show()
