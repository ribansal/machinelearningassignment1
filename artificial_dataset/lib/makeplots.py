import seaborn as sns; sns.set(style="ticks", color_codes=True)
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt

def run_heatmap(X,y):
    X_pd = pd.DataFrame(X, columns=['Feature1', 'Feature2', 'Feature3', 'Feature4', 'Feature5'])
    corrmat = X_pd.corr()
    #print(corrmat)
    hmap = sns.heatmap(corrmat, square=True, linewidths=.5)
    print(hmap.get_ylim())
    hmap.set_ylim(5.0, 0)
    plt.show()
