from drawcurves import *
from sklearn.metrics import classification_report, confusion_matrix

def run_dt(X_train, X_test, y_train, y_test):
    np.random.seed(151)
    log = open("logfileDT.txt", "w")

    log.write("\n")
    log.write("*"*80)
    log.write("\ntraining set size is {}\ntest set size is {}".format(str(X_train.shape), str(X_test.shape)))
    log.write("\ntraining set output size is {}\ntest set output size is {}".format(str(y_train.shape), str(y_test.shape)))

    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(criterion="gini"))

    #learning curve
    draw_learning_curve(pr_pipe, X_train, y_train, log, "DT")
    
    params = {}
    #plot a validation curve
    print(sorted(pr_pipe.get_params().keys()))
    param_name = "decisiontreeclassifier__criterion"
    param_range = ["gini", "entropy"]
    criterion_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['criterion'] = criterion_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_range = list(range(1,11,1)) 
    param_name = "decisiontreeclassifier__min_samples_leaf"
    min_samples_leaf_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['min_samples_leaf'] = min_samples_leaf_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_range = list(range(2,151,5))
    param_name = "decisiontreeclassifier__max_leaf_nodes"
    max_leaf_nodes_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['max_leaf_nodes'] = max_leaf_nodes_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_name = "decisiontreeclassifier__max_depth"
    param_range = list(range(1,30,1))
    param_name = "decisiontreeclassifier__max_depth"
    max_depth_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['max_depth'] = max_depth_opt_value
    
        

    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_range = 10. ** np.arange(-5, -1)
    param_name = "decisiontreeclassifier__min_samples_split"
    min_samples_split_opt_value = draw_logvalidation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['min_samples_split'] = min_samples_split_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    param_range = np.arange(0.,1.0, 0.1)
    param_name = "decisiontreeclassifier__min_impurity_decrease"
    min_impurity_decrease_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['min_impurity_decrease'] = min_impurity_decrease_opt_value
    
    #params = {}
    #params['criterion'] = criterion_opt_value
    pr_pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier(**params))
    
    pr_pipe.fit(X_train, y_train)
    
    #predict on test features
    prediction = pr_pipe.predict(X_test)
    
    #accuracy
    print("prediction accuracy is ", pr_pipe.score(X_test, y_test)*100, "%")
    log.write("prediction accuracy in DT is {} \n".format(pr_pipe.score(X_test, y_test)*100))
    
    #writing confusion matrix
    predictions = pr_pipe.predict(X_train)
    print(predictions)
    print(confusion_matrix(y_train,predictions))
    print(classification_report(y_train,predictions))
    log.write("confusion and classification on training data\n")
    log.write("%s \n"%confusion_matrix(y_train,predictions))
    log.write("%s \n"%classification_report(y_train,predictions))
    predictions = pr_pipe.predict(X_test)
    print(confusion_matrix(y_test,predictions))
    print(classification_report(y_test,predictions))
    log.write("confusion and classification on test data\n")
    log.write("%s \n"%confusion_matrix(y_test,predictions))
    log.write("%s \n"%classification_report(y_test,predictions))


    log.close()