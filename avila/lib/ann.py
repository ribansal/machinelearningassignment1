from sklearn.neural_network import MLPClassifier
from drawcurves import *

def run_ann(X_train, X_test, y_train, y_test):
    #alpha value range 10.0 ** -np.arange(1, 7)
    np.random.seed(151)
    log = open("logfileNN.txt", "w")
    
    log.write("\n")
    log.write("*"*80)
    log.write("\ntraining set size is {}\ntest set size is {}".format(str(X_train.shape), str(X_test.shape)))
    log.write("\ntraining set output size is {}\ntest set output size is {}".format(str(y_train.shape), str(y_test.shape)))
    
    mlpsolver='adam'
    #mlpsolver = 'lbfgs'
    pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(solver=mlpsolver, random_state=10, max_iter=1500, early_stopping=True))

    draw_learning_curve(pr_pipe, X_train, y_train, log, 'ann')
    
    params = {}
    params['solver'] = mlpsolver
    params['random_state'] = 10
    params['max_iter'] = 500
    params['early_stopping']=True
    
        
    pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
    #param_range = ['lbfgs', 'sgd', 'adam']
    param_range = ['sgd', 'adam']
    param_name = "mlpclassifier__solver"
    param_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['solver'] = param_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
    param_range = ['identity', 'logistic', 'tanh', 'relu']
    param_name = "mlpclassifier__activation"
    param_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['activation'] = param_opt_value
    
    
    if params['solver'] == 'sgd':
        pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
        param_range = ['constant', 'invscaling', 'adaptive']
        param_name = "mlpclassifier__learning_rate"
        param_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
        params['learning_rate'] = param_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
    param_range = 10.0 ** np.arange(-6, 0)
    param_name = "mlpclassifier__alpha"
    param_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['alpha'] = param_opt_value
    
    
    if params['solver'] == 'sgd' or params['solver'] == 'adam':
        pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
        param_range = [0.0001,0.001,0.01,0.1]
        param_name = "mlpclassifier__learning_rate_init"
        param_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
        params['learning_rate_init'] = param_opt_value
    
    
    pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
    param_name2 = 'mlpclassifier__hidden_layer_sizes'
    param_range2 = np.arange(20,101,20)
    #param_range2 = [(100,), (100,100,), (100,100,100,), (100,100,100,100,), (100,100,100,100,100,) ]
    param_opt_value2 = draw_validation_curve(pr_pipe, param_range2, param_name2, X_train, y_train, log)
    params['hidden_layer_sizes'] = param_opt_value2
    slv = param_opt_value2
    
    pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
    param_name2 = 'mlpclassifier__hidden_layer_sizes'
    param_range2 = []
    #for i in range(10,101,10):
        #param_range2.append((slv,i))
    param_range2 = [(slv,10), (slv,40,), (slv,70,), (slv,100,) ]
    #param_range2 = [(10,), (10,10,), (10,10,10,), (10,10,10,10,), (10,10,10,10,10,), (10,10,10,10,10,10,) ]
    param_opt_value2 = draw_validation_curve(pr_pipe, param_range2, param_name2, X_train, y_train, log)
    params['hidden_layer_sizes'] = param_opt_value2
    slv1 = param_opt_value2[1]
    
    pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
    param_name2 = 'mlpclassifier__hidden_layer_sizes'
    param_range2 = []
    param_range2 = [(slv,slv1,20), (slv,slv1,40,), (slv,slv1,60,), (slv,slv1,80,), (slv,slv1,100,) ]
    #for i in range(10,101,10):
    #    param_range2.append((slv,slv1,i))
    
    param_opt_value2 = draw_validation_curve(pr_pipe, param_range2, param_name2, X_train, y_train, log)
    params['hidden_layer_sizes'] = param_opt_value2
    slv2 = param_opt_value2[2]
      
    
    pr_pipe = make_pipeline(StandardScaler(), MLPClassifier(**params))
    #print(sorted(pr_pipe.get_params().keys()))
    
    
    pr_pipe.fit(X_train, y_train)
    print(pr_pipe)
    #predict on test features
    prediction = pr_pipe.predict(X_test)
    
    #accuracy
    print("prediction accuracy is ", pr_pipe.score(X_test, y_test)*100, "%")
    log.write("prediction accuracy using {} solver in MLPClassifier is {} %".format(mlpsolver, pr_pipe.score(X_test, y_test)*100))
    
    
    #creating a curve for max_iter vs accuracy
    X_train_1, X_test_1, y_train_1, y_test_1 = train_test_split(X_train, y_train, test_size=0.2, random_state=1)
    iter_accuracy = []
    iter_range = np.arange(10,210,10)
    scaler = StandardScaler()
    scaler.fit(X_train_1)
    X_train_1 = scaler.transform(X_train_1)
    X_test_1 = scaler.transform(X_test_1)
    #classifier = MLPClassifier(solver=mlpsolver, random_state=10, max_iter=1, early_stopping=True, hidden_layer_sizes=(100,100,100), alpha=0.0001)
    #classifier = MLPClassifier(**params)
    
    #params['solver'] = 'adam'
    #params['activation'] = 'relu'
    #params['alpha'] = 0.0001
    #params['learning_rate_init'] = 0.01
    #params['hidden_layer_sizes'] = (50,50,45,)
    
    
    for i in iter_range:
      params['max_iter'] = i
      classifier = MLPClassifier(**params)
      classifier.fit(X_train_1, y_train_1)
      prediction = classifier.predict(X_test_1)
      iter_accuracy.append(classifier.score(X_test_1, y_test_1)*100)
      #classifier.max_iter +=10
    
    
    log.write("\niter accruacy is")
    log.write(str(iter_accuracy))
    
    plt.figure()
    plt.plot(iter_range, iter_accuracy, color='green', linestyle='-', marker='s', 
                    markersize=5, label='validation accuracy')
    
    xlabel ="Number of iterations"
    plt.grid()
    plt.xlabel(xlabel)
    plt.ylabel('Accuracy')
    plt.legend(loc='best')
    #plt.xlim([0.00001, 0.01])
    #plt.show()
    filename = "ItervsAccuracy.png"
    plt.savefig(filename, bbox_inches='tight')
    
    
    
    log.close()
