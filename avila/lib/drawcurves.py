import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn import svm
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import tree
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import learning_curve
from sklearn.model_selection import validation_curve
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV

def draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log):
        log.write("\nMaking a validation curve for {} {}".format(param_name, str(param_range)))

        train_scores, test_scores = validation_curve(estimator=pr_pipe,
                                                X=X_train,
                                                y=y_train,
                                                param_name=param_name,
                                                param_range=param_range,
                                                cv=5)
        train_mean = np.mean(train_scores, axis=1)
        train_std = np.std(train_scores, axis=1)
        test_mean = np.mean(test_scores, axis=1)
        test_std = np.std(test_scores, axis=1)
        log.write("\ntrain mean =")
        log.write(str(train_mean))
        log.write("\ntest mean = ")
        log.write(str(test_mean))
        plt.figure()
        if(isinstance(param_range[0], tuple)):
           param_range_str = [ str(x) for x in param_range ]
        else :
            param_range_str = param_range
        plt.plot(param_range_str, train_mean, color='red', marker='o', markersize=5, 
                label='training accuracy')

        plt.fill_between(param_range_str, 
                        train_mean + train_std,
                        train_mean - train_std,
                        alpha=0.15, color='red')
        
        plt.plot(param_range_str, test_mean, color='green', linestyle='--', marker='s', 
                markersize=5, label='validation accuracy')
        plt.fill_between(param_range_str, 
                        test_mean + test_std,
                        test_mean - test_std,
                        alpha=0.15, color='green')
        xlabel = param_name[param_name.find("__")+2:]
        plt.grid()
        plt.xlabel(xlabel)
        plt.ylabel('Accuracy')
        plt.legend(loc='best')
        #plt.xlim([0.00001, 0.01])
        #plt.show()
        filename = param_name + ".png"
        plt.savefig(filename, bbox_inches='tight')
        res=np.where(test_mean == np.amax(test_mean))
        print(res, res[0][0])

        xlabel_opt_value = param_range[res[0][0]]
        print("{} maximum value from vc is {}".format(xlabel,xlabel_opt_value))
        log.write("{} maximum value from vc is {}".format(xlabel,xlabel_opt_value))
        
        
        return xlabel_opt_value

#making a learning curve
def draw_learning_curve(pr_pipe, X_train, y_train, log, classifier):
        log.write("\nMaking a learning curve")


        train_sizes, train_scores, test_scores = learning_curve(estimator=pr_pipe,
                                                                X=X_train,
                                                                y=y_train,
                                                                train_sizes=np.linspace(
                                                                0.1,1.0,10),
                                                                cv=5,
                                                                shuffle=True,
                                                                n_jobs=-1)
        train_mean = np.mean(train_scores, axis=1)
        train_std = np.std(train_scores, axis=1)
        test_mean = np.mean(test_scores, axis=1)
        test_std = np.std(test_scores, axis=1)
        log.write("\ntrain mean =")
        log.write(str(train_mean))
        log.write("\ntest mean = ")
        log.write(str(test_mean))
        print("train_mean ", train_mean)
        print("test_mean ", test_mean)
        plt.figure()
        plt.plot(train_sizes, train_mean, color='red', marker='o', markersize=5, 
                label='training accuracy')
        plt.fill_between(train_sizes, 
                        train_mean + train_std,
                        train_mean - train_std,
                        alpha=0.15, color='red')
        plt.plot(train_sizes, test_mean, color='green', marker='s', 
                markersize=5, label='validation accuracy')
        plt.fill_between(train_sizes, 
                        test_mean + test_std,
                        test_mean - test_std,
                        alpha=0.15, color='green')
        plt.grid()
        plt.xlabel('Number of training samples')
        plt.ylabel('Accuracy')
        plt.legend(loc='best')
        #plt.ylim([0.8, 1.0])
        #plt.show()
        filename = "LC_" +classifier + ".png"
        plt.savefig(filename, bbox_inches='tight')