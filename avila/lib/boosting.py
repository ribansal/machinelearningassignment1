import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import learning_curve
from sklearn.model_selection import validation_curve
from drawcurves import *

def run_boosting(X_train, X_test, y_train, y_test):

    log = open("logfileboosting.txt", "w")
    #preprocessing
    log.write("\n")
    log.write("*"*80)
    log.write("\ntraining set size is {}\ntest set size is {}".format(str(X_train.shape), str(X_test.shape)))
    log.write("\ntraining set output size is {}\ntest set output size is {}".format(str(y_train.shape), str(y_test.shape)))
    
    
    #clf = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(max_depth=24), random_state=10)
    clf = AdaBoostClassifier(random_state=10, base_estimator=DecisionTreeClassifier())
    
    pr_pipe = make_pipeline(StandardScaler(), clf)
    #learning curve
    draw_learning_curve(pr_pipe, X_train, y_train, log, "boosting")
    
    params = {}
    params['random_state']=10
    params['base_estimator'] = DecisionTreeClassifier()
    
    
    #validation curve
    pr_pipe = make_pipeline(StandardScaler(), AdaBoostClassifier(**params))
    param_name = "adaboostclassifier__base_estimator__max_depth"
    param_range = np.arange(1, 21, 2)
    param_opt_value = draw_validation_curve(pr_pipe, param_range, param_name, X_train, y_train, log)
    params['base_estimator'] = DecisionTreeClassifier(max_depth=param_opt_value)
    
    #param_opt_value=12
    
    #validation curve
    pr_pipe = make_pipeline(StandardScaler(), AdaBoostClassifier(**params))
    #pr_pipe = make_pipeline(StandardScaler(), AdaBoostClassifier(random_state=10, base_estimator=DecisionTreeClassifier(max_depth=param_opt_value)))
    param_name1 = "adaboostclassifier__n_estimators"
    param_range1 = np.arange(20, 101, 5)
    param_opt_value1 = draw_validation_curve(pr_pipe, param_range1, param_name1, X_train, y_train, log)
    params['n_estimators']= param_opt_value1
    
    #validation curve 2
    pr_pipe = make_pipeline(StandardScaler(), AdaBoostClassifier(**params))
    param_name2 = 'adaboostclassifier__learning_rate'
    param_range2 = np.arange(0.5, 5.0, 0.5)
    param_opt_value2 = draw_validation_curve(pr_pipe, param_range2, param_name2, X_train, y_train, log)
    params['learning_rate'] = param_opt_value2
    
    #making classifier with best values
    pr_pipe = make_pipeline(StandardScaler(), AdaBoostClassifier(**params))
    
    pr_pipe.fit(X_train, y_train)
    
    #predict on test features
    prediction = pr_pipe.predict(X_test)
    
    #accuracy
    print("prediction accuracy is ", pr_pipe.score(X_test, y_test)*100, "%")
    log.write("final prediction accuracy with n_estimator={} and learning rate={} is {} %".format(param_opt_value, param_opt_value2, pr_pipe.score(X_test, y_test)*100))
    
    log.close()
